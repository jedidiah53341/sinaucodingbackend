package com.sinaukoding.aplikasiSpring.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serial;

@Entity
@Table(name="Book")
@Setter
@Getter
@NoArgsConstructor

public class Book extends BaseEntity<Book> {


    @Serial
    private static final long serialVersionUID = -6758128672240989454L;
    @Column(name="title", columnDefinition = "VARCHAR(150)")
    private String title;

    @Column(name="isbn")
    private String isbn;

    @Column(name="author")
    private String author;

    @Column(name="publisher")
    private String publisher;

    @Column(name="description")
    private String description;



}
