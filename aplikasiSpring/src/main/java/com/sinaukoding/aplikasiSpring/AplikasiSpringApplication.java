package com.sinaukoding.aplikasiSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiSpringApplication.class, args);
	}

}
